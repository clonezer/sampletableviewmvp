//
//  MyFeedUser.swift
//  MyTableViewPattern
//
//  Created by Peerasak Unsakon on 1/28/20.
//  Copyright © 2020 Peerasak Unsakon. All rights reserved.
//

import Foundation

struct MyFeedUser: Codable {
    let fullname: String
    let nickname: String
    let profileImageName: String
    let quote: String
}
