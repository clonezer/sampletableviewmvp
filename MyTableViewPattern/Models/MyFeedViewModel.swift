//
//  MyFeedViewModel.swift
//  MyTableViewPattern
//
//  Created by Peerasak Unsakon on 1/28/20.
//  Copyright (c) 2020 Peerasak Unsakon. All rights reserved.
//
//  This file was generated by the 🐝 MVP generator
//

import UIKit

struct MyFeedSectionData {
    var title: String
    var cellDatas: [MyFeedCellData]
}

struct MyFeedCellData {
    var title: String
    var cellType: String
    var height: CGFloat
    var payload: Any?
}
