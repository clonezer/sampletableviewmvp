//
//  Extensions.swift
//  MyTableViewPattern
//
//  Created by Peerasak Unsakon on 4/2/2563 BE.
//  Copyright © 2563 Peerasak Unsakon. All rights reserved.
//

import Foundation
import UIKit

protocol ReusableView {
    static var reuseIdentifier: String { get }
}

extension ReusableView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: ReusableView {}

extension UITableView {
    func register<T: UITableViewCell>(_: T.Type, reuseIdentifier: String? = nil) {
        self.register(T.self, forCellReuseIdentifier: reuseIdentifier ?? String(describing: T.self))
    }
    
    func registerNib<T: UITableViewCell>(_: T.Type, reuseIdentifier: String? = nil) {
        let identifier = reuseIdentifier ?? String(describing: T.self)
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellReuseIdentifier: identifier)
    }
    
    func registerNibNames(nibNames: [String]) {
        nibNames.forEach { name in
            let nib = UINib(nibName: name, bundle: nil)
            self.register(nib, forCellReuseIdentifier: name)
        }
    }

    func dequeue<T: UITableViewCell>(_: T.Type, for indexPath: IndexPath) -> T {
        let identifier = String(describing: T.self)
        guard
            let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? T
            else { fatalError("Could not deque cell with type \(T.self)") }
        return cell
    }
    
    func dequeueCell<T: UITableViewCell>(reuseIdentifier identifier: String, for indexPath: IndexPath) -> T {
        guard
            let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? T
            else { fatalError("Could not deque cell with type \(T.self)") }
        return cell
    }
}
