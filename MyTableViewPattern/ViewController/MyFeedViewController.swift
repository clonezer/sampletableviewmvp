//
//  MyFeedViewController.swift
//  MyTableViewPattern
//
//  Created by Peerasak Unsakon on 1/28/20.
//  Copyright (c) 2020 Peerasak Unsakon. All rights reserved.
//
//  This file was generated by the 🐝 MVP generator
//

import UIKit

final class MyFeedViewController: UIViewController {

    // MARK: - Public properties -
    
    var presenter: MyFeedPresenter!

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupPresenter()
    }
    
    // MARK: - Private functions -
    
    private func setupViews() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.registerNibNames(nibNames: [MyFeedNameCell.reuseIdentifier, MyFeedTypeACell.reuseIdentifier, MyFeedTypeBCell.reuseIdentifier])
    }
    
    private func setupPresenter() {
        self.presenter = MyFeedPresenter(viewController: self)
        self.presenter.fetchData()
    }
}

// MARK: - Extensions -

extension MyFeedViewController: MyFeedViewDelegate {
    func finishLoading() {
        print("loaded")
        self.tableView.reloadData()
    }

    func startLoading() {
        print("loading ...")
    }
    
    func serviceError(_ error: Error) {
        print("error")
    }
}

extension MyFeedViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.presenter.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.sections[section].cellDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = self.presenter.sections[indexPath.section].cellDatas[indexPath.row]
        let cell: MyFeedBaseCell = tableView.dequeueCell(reuseIdentifier: cellData.cellType, for: indexPath)
        cell.setup(self, payload: cellData.payload)
        return cell
    }
}

extension MyFeedViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        #if DEBUG
        let cellData = self.presenter.sections[indexPath.section].cellDatas[indexPath.row]
        print(cellData)
        #endif
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.presenter.sections[indexPath.section].cellDatas[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionData = self.presenter.sections[section]
        return (sectionData.title != "" ? 45.0 : 0.1)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionData = self.presenter.sections[section]
        
        guard sectionData.title != "" else { return nil }
        
        let headerView = MyFeedSectionHeaderView.loadFromNib()
        headerView?.titleLabel.text = sectionData.title
        
        return headerView
    }
}

extension MyFeedViewController: MyFeedNameCellDelegate {
    func subscribeButtonDidTapped(_ sender: Any?) {
        print("SUBSCRIBE DID TAPPED")
    }
}
