//
//  MyFeedTypeACell.swift
//  MyTableViewPattern
//
//  Created by Peerasak Unsakon on 1/28/20.
//  Copyright © 2020 Peerasak Unsakon. All rights reserved.
//

import UIKit

class MyFeedTypeACell: MyFeedBaseCell {
    
    @IBOutlet weak var contentLabel: UILabel!
    
    override func setAppearances() {
        if let user = payload as? MyFeedUser {
            self.contentLabel.text = user.quote
        }
    }
}

