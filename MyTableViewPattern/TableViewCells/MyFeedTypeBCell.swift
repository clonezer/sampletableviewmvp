//
//  MyFeedTypeBCell.swift
//  MyTableViewPattern
//
//  Created by Peerasak Unsakon on 1/28/20.
//  Copyright © 2020 Peerasak Unsakon. All rights reserved.
//

import UIKit

class MyFeedTypeBCell: MyFeedBaseCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    override func setAppearances() {
        if let user = payload as? MyFeedUser, let image = UIImage(named: user.profileImageName) {
            self.profileImageView.image = image
        }
    }
}


